# Rise of Industry: Australia mods

This is the source code for mods that add Australian place names to the video game [Rise of Industry](https://www.riseofindustry.com/).

The easiest way to use these mods is to subscribe to them on their Steam Workshop pages:

* __Australian Towns:__ https://steamcommunity.com/sharedfiles/filedetails/?id=1739128150
* __Australian Federal Electorates:__ https://steamcommunity.com/sharedfiles/filedetails/?id=1739119495
